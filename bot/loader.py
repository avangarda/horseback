# from horseback import database
# import horseback.database
# from horseback.settings import env_config
from horseback.database import db, orm, User, Event, SignUp, setup_db
import gspread
import uuid
import datetime
from oauth2client.service_account import ServiceAccountCredentials
from horseback.settings import BotConfig
from time import sleep


def otworz_arkusz(json_keyfile, google_sheet_url):
    scope = ["https://spreadsheets.google.com/feeds"]
    credentials = ServiceAccountCredentials.from_json_keyfile_name(json_keyfile, scope)
    gc = gspread.authorize(credentials)
    sesje = gc.open_by_url(google_sheet_url).sheet1
    return sesje


def wczytaj_sesje(arkusz):
    # global value
    sesje = arkusz
    wszystkie = sesje.get_all_records()
    for rekord in wszystkie:
        sesja = translate_gsheets_fields(rekord)
        yield sesja


def translate_gsheets_fields(rekord):
    sesja = dict()
    temp = dict()
    organizator = dict()
    temp["status"] = str.upper(rekord["Status"])
    organizator["email"] = rekord["Email Address"]
    organizator["discord"] = rekord["Nick do Discorda"]
    organizator["name"] = rekord["Prowadzący"]
    organizator["level"] = 1
    organizator["roll20"] = rekord["Adres email do Roll20"]
    sesja["organized_by"] = rekord["Prowadzący"]
    organizator["organisation"] = rekord["Organizacja"]
    sesja["name"] = rekord["Tytuł"]
    sesja["description"] = rekord["Opis"]
    sesja["game"] = rekord["System/świat"]
    sesja["keywords"] = rekord[
        "Podaj słowa kluczowe, skojarzenia związane z Twoją sesją"
    ]
    sesja["triggers"] = rekord["Triggery"]
    sesja["min_attendees"] = rekord["Minimalna liczba graczy"]
    sesja["max_attendees"] = rekord["Maksymalna liczba graczy"]
    sesja["newbie_friendly"] = rekord["Czy sesja jest przyjazna dla początkujących?"]
    sesja["setting_familiarity_required"] = rekord["Czy wymagasz znajomości świata?"]
    sesja["age_restrictions"] = rekord["Ograniczenia wiekowe"]
    sesja["duration"] = rekord["Czas trwania"]
    # sesja['uczestnicy'] = rekord['Przygotowanie uczestników']
    sesja["safety_tools"] = rekord["Narzędzia bezpieczeństwa"]
    temp["Timestamp"] = rekord["Timestamp"]
    # sesja['godzina'] = rekord['Jaka godzina Ci odpowiada?']
    sesja["uuid"] = rekord["ID Sesji w systemie"]
    sesja["event_type"] = "rpg"

    try:
        # czas = rekord['Jaki dzień na prowadzenie sesji Ci odpowiada'] + " " + rekord['Jaka godzina Ci odpowiada?']
        czas = rekord["Dzień"] + " " + rekord["Godzina"]
        sesja["when"] = datetime.datetime.strptime(str(czas), "%Y-%m-%d %H:%M")
    except:
        sesja["when"] = None

    return sesja, organizator, temp


def ustaw_uuid_sesji(arkusz, sesja, uuid):
    wiersz = arkusz.find(sesja["Timestamp"]).row
    kolumna = arkusz.find("ID Sesji w systemie").col
    arkusz.update_cell(row=wiersz, col=kolumna, value=str(uuid))


def ustaw_status_sesji(arkusz, uuid_sesji, status):
    wiersz = arkusz.find(str(uuid_sesji)).row
    kolumna = arkusz.find("Status").col
    arkusz.update_cell(row=wiersz, col=kolumna, value=status)


# json_keyfile = "menadzersesji2-783849b7f274.json"
# gsheeturl = 'https://docs.google.com/spreadsheets/d/1gF0huKERTF-eAtLmdPZrp_AUTni_jU4Sc5GGO37s8Xs/'


def laduj_sesje():
    arkusz = otworz_arkusz(
        json_keyfile=BotConfig.json_keyfile, google_sheet_url=BotConfig.gsheeturl
    )
    setup_db()
    with orm.db_session:
        for sesja, organizator, temp in wczytaj_sesje(arkusz):
            if sesja["when"]:
                if temp["status"] == "LOAD":
                    print("Processing: {}".format(sesja["name"]))
                    if sesja["uuid"] == "":
                        sesja["uuid"] = uuid.uuid4()
                        user = User.update_or_create(
                            email=organizator["email"],
                            discord=organizator["discord"],
                            name=sesja["organized_by"],
                            level=1,
                        )
                        sesja["organized_by"] = user
                        event = Event(**sesja)
                        ustaw_uuid_sesji(arkusz=arkusz, sesja=temp, uuid=event.uuid)
                        try:
                            ustaw_status_sesji(
                                arkusz=arkusz, uuid_sesji=sesja["uuid"], status="LOADED"
                            )
                        except Exception as e:
                            print(
                                "Coś poszło nie tak podczas ustawiania statusu sesji: {}, błąd: {}".format(
                                    sesja["name"], e
                                )
                            )
                        db.commit()
                        sleep(3)
                    else:
                        event = Event.get(uuid=sesja["uuid"])
                        user = User.update_or_create(
                            email=organizator["email"],
                            discord=organizator["discord"],
                            name=sesja["organized_by"],
                            level=1,
                        )
                        sesja["organized_by"] = user
                        if event:
                            event.set(**sesja)
                        else:
                            event = Event(**sesja)
                        try:
                            ustaw_status_sesji(
                                arkusz=arkusz,
                                uuid_sesji=sesja["uuid"],
                                status="UPDATED",
                            )
                        except Exception as e:
                            print(
                                "Coś poszło nie tak podczas ustawiania statusu sesji: {}, błąd: {}".format(
                                    sesja["name"], e
                                )
                            )
                        db.commit()
                        sleep(1)
    return 0


if __name__ == "__main__":
    # setup_db()
    # with orm.db_session:
    #     for i.all in Event.get_items():
    #         print(i)
    laduj_sesje()
    print("Elo")
