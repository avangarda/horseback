from horseback.database import db, orm, User, Event, SignUp, setup_db
import datetime
from horseback.settings import BotConfig

import discord
import random
import os
from pony.orm import select


def get_rpg_sessions():
    setup_db()
    event_type = "rpg"
    with orm.db_session:
        events = Event.get_items(event_type=event_type)

        # print(type(events))
        # sessions = []
        for element in events.items():
            mg = User.email(element.organized_by)
            user = User.events_organized
            if element in user:
                print(user)
            yield (element)


# def get_rpg_sessions_short():
#     setup_db()
#     event_type = 'rpg'
#     with orm.db_session:
#         events = Event.get_items(event_type=event_type)
#         # print(type(events))
#         # sessions = []
#         for element in events:
#             yield (element)

# yield(events.items())


def select_rpg_sessions():
    setup_db()
    event_type = "rpg"
    with orm.db_session:
        sessions = select(s for s in Event)
        for s in sessions:
            # print(s.organized_by.name)
            # mg = User[s.organized_by]
            # print(mg.name)
            # print(s.name)
            yield s


if __name__ == "__main__":
    print("Elo")
    for session in select_rpg_sessions():
        print(
            "ID sesji: {}, Tytuł: {}, MG: {} Discord: {}".format(
                session.uuid,
                session.name,
                session.organized_by.name,
                session.organized_by.discord,
            )
        )
    # for i in select_rpg_sessions():
    #     print (i.name)
    # for i in get_rpg_sessions():
    #     print(i)
    # print(i)
    # print(type(i))
    # print(i[0])
    # print("UUID: {}".format())
    # exit(0)
    # for key in get_rpg_sessions().keys():
    #     print(key)
    #     print(get_rpg_sessions().values()[key])
    # print(get_rpg_sessions()[key])
