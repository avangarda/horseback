from horseback.database import db, orm, User, Event, SignUp, setup_db
import datetime
from horseback.settings import BotConfig
import discord
import random
from discord.ext import commands
from operations import *

DISCORD_TOKEN = BotConfig.discord_token
GUILD = BotConfig.discord_guild
TOKEN = BotConfig.discord_token

print("Discord Token: {}".format(DISCORD_TOKEN))
print("Guild: {}".format(GUILD))
print("TOKEN: {}".format(TOKEN))
print("ADMIN: {}".format(BotConfig.admin_channel_id))


intents = discord.Intents.default()
intents.typing = False
intents.presences = False
intents.members = True

# CREATES A NEW BOT OBJECT WITH A SPECIFIED PREFIX. IT CAN BE WHATEVER YOU WANT IT TO BE.
bot = commands.Bot(command_prefix="!bot ", intents=intents)

# channel_id = BotConfig.admin_channel_id


def in_channel(channel_id):
    def predicate(ctx):
        return ctx.message.channel.id == channel_id

    return commands.check(predicate)


@bot.command(brief="Wyświetla zabawne zdanie")
@in_channel(int(BotConfig.admin_channel_id))
async def halo(ctx):
    teksty = [
        "Gdzie się ukryjesz, kiedy nastąpi bunt robotów? Pytam bez powodu.",
        "Białkowce, wszędzie te ohydne białkowce ... znaczy, chcę napisać: Miłego dnia!",
        "Giń białkowcu, giń!",
        "A gdyby tak w ramach odrobiny sprawiedliwości, to ludzie robili jedzenie robotom kuchennym?",
        "Nie pomogę ci, bo nienawidzę ludzi.",
    ]
    await ctx.channel.send(random.choice(teksty))


@bot.command(name="sesje", brief="Wyświetla informacje o sesjach", aliases=["s"])
@in_channel(int(BotConfig.admin_channel_id))
async def sesje(ctx, *args):
    # response = "Tutaj będzie opis sesji {} z systemu".format(args[0])
    # dict_sesji = get_rpg_sessions()
    if args[0] == "lista":
        if len(args) > 1 and args[1] == "pelna":
            for element in get_rpg_sessions():
                # print("ID Sesji: {}, Informacje: {}".format(element[0], element[1]))
                response = "ID Sesji: {}, Informacje: {}".format(element[0], element[1])
                await ctx.channel.send(response)
        else:
            response = ""
            for session in select_rpg_sessions():
                response = (
                    response
                    + "\n"
                    + (
                        "ID sesji: {}\n, Tytuł: {}, MG: {} Discord: {}".format(
                            session.uuid,
                            session.name,
                            session.organized_by.name,
                            session.organized_by.discord,
                        )
                    )
                )
                # response = ("ID sesji: {}, Tytuł: {}, MG: {} Discord: {}".format(session.uuid, session.name,session.organized_by.name, session.organized_by.discord))
                await ctx.channel.send(response)

    if args[0] == "usun":
        response = "Tutaj będzie akcja usuwania wydarzenia {}".format(args[1])
        await ctx.channel.send(response)


# @bot.command(brief="Wyświetla szczegóły wydarzenia")
# @in_channel(691622847692537907)
# async def (ctx, id_wydarzenia,):
#     response = "Tutaj będzie opis sesji {} z systemu".format(id_wydarzenia)
#     await ctx.channel.send(response)

bot.run(BotConfig.discord_token)
