# Required environment variables

The following environment variables must be provided:

 - **DOTENV_PATH:** The only one you need to set as an actual environment variable. If set, we will to try to load all other environment variables from this path. If not, we'll see if `.env` file is sitting next to the `main.py` file.

Authentication-related:

 - **GOOGLE_CLIENT_ID:** Client ID for google auth
 - **GOOGLE_CLIENT_SECRET:** Client secret for google auth

Database-related:

 - **DB_PROVIDER:** Database provider, e.g. `sqlite` or `postgres`. For more provider types, refer to [this](https://docs.ponyorm.org/api_reference.html#supported-databases) document.
 - **DB_USER:** Username for the database (ignored for sqlite)
 - **DB_PASSWORD:** Password for the above user (ignored for sqlite)
 - **DB_HOST:** Where the database is hosted (ignored for sqlite)
 - **DB_NAME:** Name of the database, or in sqlite case, of the file
