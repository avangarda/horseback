from __future__ import annotations

from datetime import datetime
from email.policy import default
from enum import Enum
from typing import Any, List, Optional, Union

from pydantic import BaseModel

try:
    from horseback.database import Event, User
except:
    from database import Event, User


class Message(BaseModel):
    class Statuses(Enum):
        success: str = "success"
        error: str = "error"

    detail: Optional[str] = None


class SessionUploadInput(BaseModel):
    encoded_file: str


class EventShortRead(BaseModel):
    name: str
    organizer: str
    url: str


class EventFullRead(EventShortRead):
    event_type: str
    organizer: str
    start: datetime
    end: datetime
    age_restrictions: str
    attending: List[Union[UserShortRead, str]] = None
    attending_count: int = 0
    wait_list: List[Union[UserShortRead, str]] = None
    waiting_count: int = 0
    capacity: int = 0
    keywords: Optional[str]
    game: Optional[str]
    triggers: Optional[str]
    newbie_friendly: Optional[str]
    setting_familiarity_required: Optional[str]
    safety_tools: Optional[str]
    description: Optional[str]
    url: Optional[str]
    registration_opened: bool


class EventAttending(BaseModel):
    attending: List[Union[UserShortRead, str]] = None


class EventWrite(BaseModel):
    name: str
    event_type: Event.EventTypes
    age_restrictions: str
    when: datetime
    duration: int
    max_attendees: int
    min_attendees: int = 1
    keywords: Optional[str]
    game: Optional[str]
    triggers: Optional[str]
    newbie_friendly: Optional[str]
    setting_familiarity_required: Optional[str]
    safety_tools: Optional[str]
    description: Optional[str]
    registration_opened: bool


class SignUpModel(BaseModel):
    user: str
    event: str
    timestamp: datetime
    on_waitlist: bool = False


class UserRead(BaseModel):
    email: str
    name: Optional[str]
    discord: Optional[str]
    roll20: Optional[str]
    organisation: Optional[str]
    organizing: List[EventFullRead]
    attending: List[Any]


class UserShortRead(BaseModel):
    email: str
    name: Optional[str]
    discord: Optional[str]
    roll20: Optional[str]
    organisation: Optional[str]


class UserWrite(BaseModel):
    name: str
    discord: Optional[str]
    roll20: Optional[str]
    organisation: Optional[str]
    level: Optional[User.PermissionLevel] = User.PermissionLevel.basic
    who: Optional[str]


class UserQuery(BaseModel):
    email: Optional[str] = None
    discord: Optional[str] = None


EventFullRead.update_forward_refs()
EventAttending.update_forward_refs()
