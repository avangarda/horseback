try:
    from horseback.main import app
except:
    from main import app

if __name__ == "__main__":
    app.run()
