from enum import Enum
import requests
from typing import Optional

from authlib.integrations.starlette_client import OAuth
from fastapi import APIRouter, HTTPException, Request, Body
from fastapi.responses import RedirectResponse

from . import orm, User

try:
    from horseback.settings import env_config
    from horseback.models import Message
except:
    from settings import env_config
    from models import Message

oauth = OAuth(env_config)

providers = {
    "google": {
        "server_metadata_url": "https://accounts.google.com/.well-known/openid-configuration",
        "client_kwargs": {"scope": "openid email profile"},
    }
}

ProviderChoices = Enum("ProviderChoices", {k: k for k in providers})

[oauth.register(name=name, **kwargs) for name, kwargs in providers.items()]

router = APIRouter()


@router.get("/login")
async def login_choices(request: Request):
    """Lists available login providers"""
    return [request.url_for("login", provider=k) for k in sorted(providers)]


@router.get("/login/{provider}")
async def login(provider: ProviderChoices, request: Request):
    """Performs login with a given provider"""
    redirect_uri = request.url_for("auth", provider=provider.value)
    return await getattr(oauth, provider.value).authorize_redirect(
        request, redirect_uri
    )


@router.post("/login/facebook", response_model=Message)
async def facebook(
    request: Request, accessToken: str = Body(None), userID: Optional[str] = Body(None)
):
    with orm.db_session:
        payload = {"access_token": accessToken, "fields": ["name,email,id"]}
        response = requests.get("https://graph.facebook.com/me", params=payload)
        if response.status_code != 200:
            return {
                "status": "error",
                "detail": "Authentication via facebook was unsuccessful",
            }
        response = response.json()
        user = User.update_or_create(
            name=response["name"],
            email=response["email"],
            level=User.PermissionLevel.basic.value,
        )
        request.session["user"] = {"email": user.email, "name": user.name}
        return {"status": "success", "detail": "/#/profile"}


@router.get("/logout")
async def logout(request: Request):
    """Logs out the current user"""
    if "user" in request.session:
        request.session.pop("user")
    return RedirectResponse("/login")


@router.get("/beepboop")
async def beepboop(request: Request):
    """Party like it's 2077"""
    if "x-horseplay" not in request.headers or not request.headers["x-horseplay"]:
        raise HTTPException(420)

    expected_token = env_config("HORSEPLAY_TOKEN")
    if request.headers["x-horseplay"] != expected_token:
        raise HTTPException(401)
    request.session["user"] = {"email": "beepboop@lmao"}
    with orm.db_session:
        User.update_or_create(
            email="beepboop",
            level=User.PermissionLevel.admin.value,  # TODO: maybe change me after 2077
        )
    return RedirectResponse("/i_am")


@router.get("/auth/{provider}")
async def auth(request: Request, provider: ProviderChoices):
    """Authenticates the user with a given provider"""
    token = await getattr(oauth, provider.value).authorize_access_token(request)
    user = await getattr(oauth, provider.value).parse_id_token(request, token)
    request.session["user"] = dict(user)
    return RedirectResponse("https://sesje.konwent.online/#/profile")
