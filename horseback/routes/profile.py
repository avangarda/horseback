import logging

from fastapi import APIRouter, Depends, Response

from . import get_user_key, UserData

try:
    from horseback.database import orm, User
    from horseback.models import UserRead, UserQuery, UserWrite
except:
    from database import orm, User
    from models import UserRead, UserQuery, UserWrite

router = APIRouter()
logger = logging.getLogger(__name__)


@router.get("/", response_model=UserRead, responses={401: {}})
async def profile(response: Response, user_data: UserData = Depends(get_user_key)):
    with orm.db_session:
        return User[user_data.key].to_dict()


@router.get("/investigating")
async def investigate_user(
    response: Response,
    user_query: UserQuery,
    user_data: UserData = Depends(get_user_key),
):
    if user_data.permissions < UserData.PermissionLevel.admin:
        response.status_code = 403
        return

    params = {k: v for k, v in user_query.to_dict().items() if v}

    if not params:
        response.status_code = 404
        return

    with orm.db_session:
        try:
            return User.get(**params).to_dict()
        except:
            logger.exception(f"Could not get user by querying {params}")
            response.status_code = 404


@router.post("/changing", response_model=UserWrite, responses={401: {}})
async def profile_update(
    new_data: UserWrite, response: Response, user_data: UserData = Depends(get_user_key)
):
    to_write = dict(new_data)

    user_key = user_data.key

    if new_data.who:
        if user_data.permissions < UserData.PermissionLevel.admin:
            response.status_code = 403
            return

        user_key = new_data.who
    to_write.pop("who", None)

    with orm.db_session:
        user = User[user_key]
        if user_data.permissions < User.PermissionLevel.admin.value:
            to_write.pop("level", None)
            if new_data.level:
                logger.warning(
                    f'User "{user_data.key}" tried to set the level to {new_data.level} for user {user.email}'
                )

        elif (
            user_key == user_data.key
            and new_data.level is not None
            and new_data.level < User.PermissionLevel.admin.value
        ):
            logger.warning(
                f"User {user_key} tried to demote themselves, this is not allowed"
            )
            to_write.pop("level", None)

        user.set(**to_write)

        return to_write


@router.delete("/removing/myself")
async def delete_profile_for_GDPR(user_data: UserData = Depends(get_user_key)):
    with orm.db_session:
        user = User[user_data.key]
        user.delete()
        return "ok :("
