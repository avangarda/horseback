from typing import NamedTuple

from fastapi import Request, Response, HTTPException

try:
    from horseback.database import orm, User
except:
    from database import orm, User


class UserData(NamedTuple):
    key: str = ""
    permissions: User.PermissionLevel = User.PermissionLevel.basic


async def get_user_key(request: Request, response: Response):
    user_data = request.session.get("user")
    if not user_data:
        raise HTTPException(status_code=401, detail="Not logged in")

    with orm.db_session:
        user = User.update_or_create(email=user_data["email"])
        if user.level < User.PermissionLevel.basic:
            raise HTTPException(status_code=403, detail="Banned lmao")
        return UserData(key=user_data["email"], permissions=user.level)
