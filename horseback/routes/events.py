import datetime
import io, csv
import logging
import base64
from typing import Any, List, Optional
import uuid

from fastapi import APIRouter, Depends, Request, Response

from . import get_user_key, UserData

try:
    from horseback.database import Event, SignUp, User, db, orm
    from horseback.delegation import Delegator
    from horseback.models import (
        EventShortRead,
        EventFullRead,
        EventWrite,
        Message,
        SignUpModel,
        EventAttending,
        SessionUploadInput,
    )
except:
    from database import Event, SignUp, User, db, orm
    from delegation import Delegator
    from models import (
        EventFullRead,
        EventShortRead,
        EventWrite,
        Message,
        SignUpModel,
        EventAttending,
        SessionUploadInput,
    )

router = APIRouter()
shared_with_profile = APIRouter()
logger = logging.getLogger(__name__)


def can_alter_event(user: User, event: Event):
    if user.level >= User.PermissionLevel.admin.value:
        return True

    return (
        user.level == User.PermissionLevel.organizer.value
        and event.organized_by == user
    ) or user.level > User.PermissionLevel.organizer


@router.post("/load_csv_file", response_model=Message)
async def upload_file_with_events(
    upload_input: SessionUploadInput,
    response: Response,
    user_data: UserData = Depends(get_user_key),
):
    if not user_data:
        return None

    if user_data.permissions < User.PermissionLevel.admin.value:
        response.status_code = 403
        return {"status": "error", "detail": "You are not an admin."}

    with orm.db_session:
        input = base64.b64decode(upload_input.encoded_file)
        input = input.decode("utf8")
        counter = 0
        with io.StringIO(input) as data:
            reader = csv.DictReader(data)
            for line in reader:
                organiser = {
                    "email": line.get("Adres e-mail"),
                    "discord": line.get("Nick do Discorda"),
                    "name": line.get("Prowadzący"),
                    "level": 1,
                    "roll20": line.get("Adres email do Roll20"),
                    "organisation": line.get("Organizacja"),
                }

                if line.get("Rafałowe") == "piątek":
                    event_date = "2022-03-04"
                elif line.get("Rafałowe") == "sobota":
                    event_date = "2022-03-05"
                else:
                    event_date = "2022-03-06"

                event_datetime = (
                    f'{event_date} {line.get("Jaka godzina Ci odpowiada?")}'
                )
                event_data = {
                    "uuid": uuid.uuid4(),
                    "name": line.get("Tytuł"),
                    "description": line.get("Opis"),
                    "game": line.get("System/świat"),
                    "keywords": line.get(
                        "Podaj słowa kluczowe, skojarzenia związane z Twoją sesją"
                    ),
                    "triggers": line.get("Triggery"),
                    "min_attendees": line.get("Minimalna liczba graczy"),
                    "max_attendees": line.get("Maksymalna liczba graczy"),
                    "newbie_friendly": line.get(
                        "Czy sesja jest przyjazna dla początkujących?"
                    ),
                    "setting_familiarity_required": line.get(
                        "Czy wymagasz znajomości świata?"
                    ),
                    "age_restrictions": line.get("Ograniczenia wiekowe"),
                    "duration": line.get("Czas trwania"),
                    "safety_tools": line.get("Narzędzia bezpieczeństwa"),
                    "when": datetime.datetime.strptime(
                        str(event_datetime), "%Y-%m-%d %H:%M:%S"
                    ),
                    "event_type": "rpg",
                    "registration_opened": True,
                }
                try:
                    user = User.update_or_create(
                        email=organiser["email"],
                        discord=organiser["discord"],
                        name=organiser["name"],
                        level=organiser["level"],
                    )
                    Event(organized_by=user, **dict(event_data))
                    counter += 1
                except:
                    pass

        return {
            "status": "success",
            "detail": f"There was {counter} new events created.",
        }


@shared_with_profile.get(
    "/i_am/attending", response_model=List[Any], responses={401: {}}
)
async def attended_by_you(
    request: Request, user_data: UserData = Depends(get_user_key)
):
    """
    Get all events the user is attending
    """
    if not user_data:
        return None

    with orm.db_session:
        user = User[user_data.key]
        return [
            {
                "url": request.url_for("event_details", event_id=x.event.uuid),
                **x.event.to_dict(),
            }
            for x in user.attending
        ]


@shared_with_profile.get(
    "/i_am/organizing", responses={401: {}}, response_model=List[EventFullRead]
)
async def organized_by_you(
    request: Request, user_data: UserData = Depends(get_user_key)
):
    """
    Get all events the user is organizing
    """
    if not user_data:
        return None

    with orm.db_session:
        user = User[user_data.key]
        return [
            {"url": request.url_for("event_details", event_id=x.uuid), **x.to_dict()}
            for x in user.events_organized
        ]


@router.get("/events")
@router.get("/events/like/{event_type}")
async def event_list(event_type: Event.EventTypes = Event.EventTypes.every.value):
    """
    Get a list of events (optionally: of a given type)
    """
    return Event.get_items(*[event_type.value if event_type else None])


@router.post("/events", response_model=Message)
async def add_event(
    event_data: EventWrite,
    request: Request,
    response: Response,
    user_data: UserData = Depends(get_user_key),
):
    if not user_data:
        return None

    if user_data.permissions < User.PermissionLevel.organizer.value:
        response.status_code = 403
        return {"status": "error", "detail": "You are not an event organizer"}

    with orm.db_session:
        user = User[user_data.key]
        event = Event(organized_by=user, **dict(event_data))
        response.status_code = 201

        # Delegator.event_added(None, EventShortRead(**event_data))

        return {
            "status": "success",
            "detail": request.url_for("event_details", event_id=event.uuid),
        }


@router.get("/events/{event_id}", response_model=Optional[EventFullRead])
async def event_details(
    event_id: uuid.UUID,
    response: Response,
    request: Request,
    user_data: UserData = Depends(get_user_key),
):
    # async def event_details(event_id: UUID, response: Response, request: Request):
    #    if not user_data:
    #        return None

    with orm.db_session:
        event = Event.get(uuid=event_id)
        if event is None:
            response.status_code = 404
            return None

        if (
            event.organized_by.email == user_data.key
            or user_data.permissions == User.PermissionLevel.admin.value
        ):
            perm = user_data.permissions
        else:
            perm = 0

        return {
            "url": request.url_for("event_details", event_id=event.uuid),
            **event.to_dict(perm),
        }


@router.patch("/events/{event_id}", response_model=Message)
async def update_event(
    event_id: uuid.UUID,
    event_data: EventWrite,
    response: Response,
    user_data: UserData = Depends(get_user_key),
):
    with orm.db_session(optimistic=False):
        event = Event.get(uuid=event_id)
        if event is None:
            response.status_code = 404
            return {"status": "error", "detail": "No such event"}

        user = User[user_data.key]

        if not can_alter_event(user, event):
            response.status_code = 403
            return {"status": "error", "detail": "You may not modify this event"}

        event.set(**dict(event_data))
        # Delegator.event_changed(old_event, EventFullRead(**event.to_dict()))
        return {"status": "success", "detail": "Event updated successfully"}


@router.post("/events/{event_id}/attendance", response_model=Message)
async def attend_event(
    event_id: uuid.UUID, response: Response, user_data: UserData = Depends(get_user_key)
):
    with orm.db_session:
        event = Event.get(uuid=event_id)
        if event is None:
            response.status_code = 404
            return {"status": "error", "detail": "No such event"}

        user = User[user_data.key]

        if event.organized_by == user:
            response.status_code = 420
            return {
                "status": "error",
                "detail": "This is your event. That you are running. Stop hogging the attendee list, what are you even doing here.",
            }

        if not event.registration_opened:
            response.status_code = 403
            return {
                "status": "error",
                "detail": "Sorry, registration for this event is closed by admin.",
            }

        SignUp.update_or_create(user=user, event=event)
        db.commit()

        # to_broadcast = SignUpModel(
        #     user=user_data.key,
        #     event=event.uuid,
        #     timestamp=event.when
        # )

        if user in event.attending:
            response.status_code = 201
            # Delegator.signup_added(None, to_broadcast)
            return {"status": "success", "detail": "You are now on the attendee list"}
        elif user in event.waiting_list:
            # to_broadcast.on_waitlist = True
            response.status_code = 200  # check
            return {
                "status": "success",
                "detail": "You are on the waiting list. If a spot clears up, you will be free to attend this event",
            }
        else:
            response.status_code = 500
            return {
                "status": "error",
                "detail": "Cannot see you on the list for this event, wtf",
            }


@router.delete(
    "/events/{event_id}/attendance",
    responses={401: {}, 403: {}, 404: {}},
    response_model=Message,
)
async def unattend_event(
    event_id: uuid.UUID,
    response: Response,
    kickee_email: Optional[str] = None,
    user_data: UserData = Depends(get_user_key),
):
    removing_yourself = kickee_email is None or kickee_email == user_data.key

    if (
        not removing_yourself
        and user_data.permissions < User.PermissionLevel.organizer.value
    ):
        response.status_code = 403
        return None

    with orm.db_session:
        try:
            event = Event[event_id]
        except:
            response.status_code = 404
            return {"status": "error", "detail": "No such event"}

        user = User[user_data.key]

        if not removing_yourself and (
            user_data.permissions < User.PermissionLevel.admin.value
            and event.organized_by != user
        ):
            response.status_code = 403
            return {
                "status": "error",
                "detail": "You do not own this event, nor are you attending it",
            }

        if not event.registration_opened:
            response.status_code = 403
            return {
                "status": "error",
                "detail": "Registration for this event is closed by admin. Contact admin to sign out.",
            }

        if removing_yourself:
            kickee = user
        else:
            try:
                kickee = User[kickee_email]
            except:
                response.status_code = 404
                return {
                    "status": "error",
                    "detail": f"Cannot kick user {kickee_email}: they do not exist",
                }

        try:
            signup = SignUp[kickee, event]
            # to_broadcast = SignUpModel(
            #     user=kickee_email,
            #     event=event.uuid,
            #     timestamp=event.when
            # )
            signup.delete()
            # Delegator.signup_removed(to_broadcast, None)
            return {
                "status": "success",
                "detail": f"User {kickee.email} is no longer attending this event.",
            }
        except:
            logger.exception(f"Could not delete a signup for {event.name} ({event_id})")
            return {
                "status": "error",
                "detail": f"Could not kick {kickee.email} from this event",
            }


@router.get("/events/{event_id}/attending", response_model=EventAttending)
async def event_attendees(
    event_id: uuid.UUID,
    response: Response,
    request: Request,
    user_data: UserData = Depends(get_user_key),
):
    with orm.db_session:
        event = Event.get(uuid=event_id)
        if event is None:
            response.status_code = 404
            return None

        if (
            event.organized_by.email == user_data.key
            or user_data.permissions == User.PermissionLevel.admin.value
        ):
            return event.to_dict(user_data.permissions)

        response.status_code = 403
        return {
            "status": "error",
            "detail": "Only admin user can get list of attending users.",
        }
