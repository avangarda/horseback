import asyncio
from multiprocessing.dummy import Pool
from typing import Coroutine, Dict, Optional, Protocol, Set, Union

from pydantic import BaseModel

try:
    from ..models import EventFullRead, EventShortRead, SignUpModel
except:
    from models import EventFullRead, EventShortRead, SignUpModel


class HandlerSignature(Protocol):
    def __call__(self, old: Optional[BaseModel], new: BaseModel) -> None:
        ...


class Delegator:
    event_bindings: Dict[str, Set[Union[HandlerSignature, Coroutine]]] = {
        event_type: set()
        for event_type in (
            "event_added",
            "event_changed",
            "event_removed",
            "signup_added",
            "signup_removed",
        )
    }

    runner_pool = Pool(8)  # add customisation to this

    @classmethod
    def _bindings_or_error(cls, event_type):
        """Return bindings for a given event type, or raise ValueError if there's no such type"""
        try:
            return cls.event_bindings[event_type]
        except KeyError:
            raise ValueError(f"No such event type: {event_type}")

    @classmethod
    def clear(cls, event_type):
        """USE WITH CAUTION: emoves all events bound to a given event type"""
        cls._bindings_or_error(event_type)
        cls.event_bindings[event_type] = set()

    @classmethod
    def unbind(cls, event_type: str, func: HandlerSignature):
        """Unbinds a handler from a given event type (if it's bound to it)"""
        cls._bindings_or_error(event_type).discard(func)

    @classmethod
    def bind(cls, event_type: str, func: HandlerSignature):
        """Bind a handler to a given event type.

        The event type must have been defined in the Delegator, otherwise a ValueError will be thrown.

        Note that since the events are collected in a set, multiple references to the same function will
        be ignored and the function will only be run once, for example:

        >>> Delegator.bind("event_added", print)
        >>> asyncio.run(Delegator.event_added("old", "new"))
        old new
        >>> Delegator.bind("event_added", print)
        >>> asyncio.run(Delegator.event_added("old", "new"))
        old new

        You can overcome this limitation with lambdas, if you *really* need to, for example:

        >>> Delegator.clear("event_added")
        >>> [Delegator.bind("event_added", lambda *a, **kw: print(a, kw)) for _ in range(3)]
        [None, None, None]
        >>> asyncio.run(Delegator.event_added("old", "new"))
        ('old', 'new') {}
        ('old', 'new') {}
        ('old', 'new') {}
        """
        cls._bindings_or_error(event_type).add(func)

    async def _execute(
        cls, old: Optional[BaseModel], new: BaseModel, event_type
    ) -> None:
        """
        Generic executor for events. Mapped to events after the function definition.

        Executes all handlers bound to a given event type, using multiprocessing.dummy.Pool
        for items that are synchronous, and asyncio.gather for ones that are not.
        """
        events = cls._bindings_or_error(event_type)
        asyncs = (x(old, new) for x in events if asyncio.iscoroutinefunction(x))
        syncs = (x for x in events if not asyncio.iscoroutinefunction(x))

        cls.runner_pool.map(lambda f: f(old, new), syncs)
        await asyncio.gather(*asyncs)

    @classmethod
    async def event_added(cls, old: None, new: EventShortRead) -> None:
        cls._execute(cls, old, new, "event_added")

    @classmethod
    async def event_changed(cls, old: EventFullRead, new: EventFullRead) -> None:
        cls._execute(cls, old, new, "event_changed")

    @classmethod
    async def event_removed(cls, old, new) -> None:
        cls._execute(cls, old, new, "event_removed")

    @classmethod
    async def signup_added(cls, old: None, new: SignUpModel) -> None:
        cls._execute(cls, old, new, "signup_added")

    @classmethod
    async def signup_removed(cls, old: SignUpModel, new: None) -> None:
        cls._execute(cls, old, new, "signup_removed")
