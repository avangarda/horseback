import os

from starlette.config import Config

dotenv_path = os.getenv("DOTENV_PATH", os.path.join(os.path.dirname(__file__), ".env"))

env_config = Config(dotenv_path)


class BotConfig(object):
    env_config = Config(dotenv_path)
    json_keyfile = "horseback.json"
    # gsheeturl = 'https://docs.google.com/spreadsheets/d/1gF0huKERTF-eAtLmdPZrp_AUTni_jU4Sc5GGO37s8Xs/'
    gsheeturl = "https://docs.google.com/spreadsheets/d/1FiI2fnJAFXQaFcFLNEXlbrlCrhmrhyzjYRj7SydKoIc/"
    discord_token = env_config("DISCORD_TOKEN", default=None)
    discord_guild = env_config("DISCORD_SERVER", default=None)
    admin_channel_id = env_config("ADMIN_CHANNEL_ID", default=None)
