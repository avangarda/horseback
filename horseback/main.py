from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from starlette.middleware.sessions import SessionMiddleware

# from fastapi.middleware.httpsredirect import HTTPSRedirectMiddleware


try:  # local
    from horseback.database import setup_db
    from horseback.routes import auth, events, profile
    from horseback.settings import env_config
except:  # dockerized
    from database import setup_db
    from routes import auth, events, profile
    from settings import env_config


setup_db()
app = FastAPI()
app.add_middleware(SessionMiddleware, secret_key=env_config("SESSION_SECRET_KEY"))
# app.add_middleware(HTTPSRedirectMiddleware)

app.include_router(auth.router, tags=["authentication"])

app.include_router(profile.router, tags=["profile"], prefix="/i_am")

app.include_router(events.shared_with_profile, tags=["profile", "events"])

app.include_router(events.router, tags=["events"])

app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        "https://avangarda.gitlab.io",
        "http://localhost:8080",
        "http://localhost:8000",
        "http://localhost:8081",
        "https://sesje.konwent.online",
        "https://sesje-test.konwent.online",
        "http://sesje.konwent.online",
        "http://sesje-test.konwent.online",
        "https://accounts.google.com",
        "https://accounts.youtube.com",
    ],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
