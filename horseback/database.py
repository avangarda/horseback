import uuid
from datetime import datetime, timedelta
# from email.policy import default
from enum import Enum, IntEnum

from pony import orm

try:
    from horseback.settings import env_config
except:
    from settings import env_config

db = orm.Database()


class UpsertMixin:
    @classmethod
    def update_or_create(cls, **kwargs):
        try:
            instance = cls[tuple(kwargs[pk_attr.name] for pk_attr in cls._pk_attrs_)]
        except orm.ObjectNotFound:
            return cls(**kwargs)
        else:
            instance.set(**kwargs)
            return instance


class User(db.Entity, UpsertMixin):
    class PermissionLevel(IntEnum):
        banned = -1
        basic = 0
        organizer = 1
        admin = 2

    email = orm.PrimaryKey(str)
    name = orm.Optional(str)
    discord = orm.Optional(str)
    roll20 = orm.Optional(str, nullable=True)
    organisation = orm.Optional(str, nullable=True)
    level = orm.Required(int, default=PermissionLevel.basic.value)

    events_organized = orm.Set("Event")
    attending = orm.Set("SignUp")

    def to_dict(self):
        return {
            "organizing": [
                x.to_dict(User.PermissionLevel.organizer.value)
                for x in self.events_organized
            ],
            "attending": [x.event.to_dict() for x in self.attending],
            **{key: getattr(self, key) for key in ("email", "name", "discord")},
        }


class Event(db.Entity):
    class EventTypes(str, Enum):
        every = ""
        rpg = "rpg"
        larp = "larp"
        prelka = "prelka"

    uuid = orm.PrimaryKey(uuid.UUID, default=uuid.uuid4)
    name = orm.Required(str)
    event_type = orm.Required(str)
    age_restrictions = orm.Required(str)
    when = orm.Required(datetime)
    duration = orm.Required(int)
    organized_by = orm.Required(User)
    max_attendees = orm.Required(int)
    min_attendees = orm.Required(int, default=0)
    keywords = orm.Optional(str)
    description = orm.Optional(str)
    game = orm.Optional(str)
    triggers = orm.Optional(str)
    newbie_friendly = orm.Optional(str)
    setting_familiarity_required = orm.Optional(str)
    safety_tools = orm.Optional(str)
    registration_opened = orm.Optional(bool, default=False, sql_default="0")
    signups = orm.Set("SignUp")

    @property
    def ends_at(self):
        return self.when + timedelta(hours=self.duration)

    @property
    def is_upcoming(self):
        return self.ends_at >= datetime.now()

    @property
    def attending(self):
        return [
            x.user
            for x in self.signups.order_by(lambda s: s.timestamp)[: self.max_attendees]
        ]

    @property
    def waiting_list(self):
        if len(self.attending) < self.max_attendees:
            return []
        return [
            x.user
            for x in self.signups.order_by(lambda s: s.timestamp)[self.max_attendees :]
        ]

    def to_dict(self, permission_level=0):
        def _get_users(lst):
            if permission_level == User.PermissionLevel.admin.value:
                return [x.to_dict() for x in lst]

            return [x.discord for x in lst]

        return {
            "name": self.name,
            "event_type": self.event_type,
            "organizer": self.organized_by.name,
            "age_restrictions": self.age_restrictions,
            "keywords": self.keywords,
            "description": self.description,
            "game": self.game,
            "triggers": self.triggers,
            "newbie_friendly": self.newbie_friendly,
            "setting_familiarity_required": self.setting_familiarity_required,
            "safety_tools": self.safety_tools,
            "start": self.when,
            "end": self.ends_at,
            "attending": _get_users(self.attending),
            "attending_count": len(self.attending),
            "wait_list": _get_users(self.waiting_list),
            "waiting_count": len(self.waiting_list),
            "capacity": self.max_attendees,
            "registration_opened": self.registration_opened,
        }

    @classmethod
    def get_items(cls, event_type=""):
        if event_type:
            condition = lambda e: e.event_type == event_type
        else:
            condition = lambda _: True
        with orm.db_session:
            return {
                e.uuid: e.to_dict()
                for e in orm.select(event for event in cls if condition(event))
            }


class SignUp(db.Entity, UpsertMixin):
    user = orm.Required(User)
    event = orm.Required(Event)
    timestamp = orm.Required(datetime, default=datetime.now)

    orm.PrimaryKey(user, event)


def setup_db():
    provider = env_config("DB_PROVIDER", default="sqlite")
    db_settings = {
        "provider": provider,
        "filename"
        if provider == "sqlite"
        else "database": env_config("DB_NAME", default="testdb"),
        **{
            x.split("_")[-1].lower(): val
            for x in ("DB_USER", "DB_PASSWORD", "DB_HOST")
            if (val := env_config(x, default=None)) is not None
        },
    }

    if provider == "sqlite":
        db_settings["create_db"] = True

    if provider == "postgres":
        db_settings["user"] = f'{db_settings["user"]}@{db_settings["host"]}'
    print(
        "Connecting to database {} as user {}".format(
            db_settings.get("database"), db_settings.get("user")
        )
    )
    db.bind(**db_settings)
    db.generate_mapping(create_tables=True)
