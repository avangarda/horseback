FROM tiangolo/uvicorn-gunicorn-fastapi:python3.8

RUN pip install poetry

WORKDIR /app
COPY poetry.lock pyproject.toml /app/

RUN poetry config virtualenvs.create false
RUN poetry install --no-interaction --no-dev

COPY ./horseback /app
